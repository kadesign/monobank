# Changelog

## 1.2.0
- Added dark mode (themes can be selected on the auth screen)
- Added support for German language
- Minor UI changes on room and auth screens

## 1.1.0
- Now using Node.js v20.12.1 and Docker Compose v2
- Updated dependencies
- Migrated to Vite from vue-cli

## 1.0.0

First version.

- Create and join bank rooms
- Transfer funds between players, from and to the bank
- See your transactions (and bank transactions if you're a banker)
- Declare bankruptcy
- Appoint new banker
- Close the bank
