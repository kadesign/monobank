FROM node:20.12.1-alpine as build-image

WORKDIR /opt/kadesign/monobank

COPY ./packages/client .
RUN yarn
RUN yarn build

###

FROM node:20.12.1-alpine as production-image

ENV PORT=3000
ENV NODE_ENV=production

WORKDIR /opt/kadesign/monobank

COPY ./packages/server .
COPY --from=build-image /opt/kadesign/monobank/dist ./app/static
RUN yarn

ENTRYPOINT [ "yarn", "start" ]
EXPOSE $PORT
