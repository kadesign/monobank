![Monobank](packages/client/public/images/logo.svg)

[![Version](https://img.shields.io/badge/version-1.2.0-blue)](https://gitlab.com/kadesign/monobank/-/tags/1.2.0)
[![Pipeline status](https://gitlab.com/kadesign/monobank/badges/1.2.0/pipeline.svg)](https://gitlab.com/kadesign/monobank/-/commits/1.2.0)
![Maintenance status](https://img.shields.io/maintenance/yes/2024)

Banking buddy for Monopoly(tm)

## How to start
### Locally

```bash
yarn dev
```

Will be started on port 3000 by default.

You can also use `compose.local.yml` to build and start the application in container. Environment variable `APP_PORT` can be set to change default port.

### On remote Docker (via Gitlab CI pipeline)
1. Set up project variables:
    
   | Variable        | Description                                           | Protected | Masked |
   |-----------------|-------------------------------------------------------|-----------|--------|
   | **APP_VERSION** | App version to deploy                                 | +         |        |
   | **APP_PORT**    | Local port on which the application will be available | +         |        |
   | **PROD_URL**    | Application URL                                       | +         |        |
   | **SERVER_IP**   | Server IP                                             | +         |        |
   | **SERVER_USER** | Server user in group `docker`                         | +         |        |
   | **ID_RSA**      | Private SSH key                                       | +         | +      |

2. Trigger pipeline, it should deploy the application to remote server
