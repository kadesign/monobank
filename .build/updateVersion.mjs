import * as fs from 'fs';

let readme = fs.readFileSync('README.md', {
  encoding: 'utf-8'
});

if (readme?.length > 0) {
  const pkg = fs.existsSync('lerna.json') ? JSON.parse(fs.readFileSync('lerna.json', {
    encoding: 'utf-8'
  })) : {
    version: process.env.npm_package_version
  };

  console.log(`Version is v${pkg.version}`);

  // [![Version](https://img.shields.io/badge/version-v<version>-blue)](https://gitlab.com/<user>/<repo>/-/tags/v<version>)
  if (/\[!\[Version]\(https:\/\/img\.shields\.io\/badge\/version-v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\d+)?-blue\)]\(https:\/\/gitlab\.com\/[a-z0-9-_]+\/[a-z0-9-_]+\/-\/tags\/v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\s+)?/.test(readme)) {
    readme = readme.replace(/badge\/version-v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\d+)?-blue/, `badge/version-${pkg.version}-blue`)
      .replace(/\/-\/tags\/v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\s+)?/, `/-/tags/${pkg.version}`);
    console.log('Version badge updated');
  }

  // [![Pipeline status](https://gitlab.com/<user>/<repo>/badges/<version>/pipeline.svg)](https://gitlab.com/<user>/<repo>/-/commits/<version>)
  if (/\[!\[Pipeline status]\(https:\/\/gitlab\.com\/[a-z0-9-_]+\/[a-z0-9-_]+\/badges\/(\d+\.\d+\.\d+)(\.?[a-z]\.?\d+)?\/pipeline\.svg\)]\(https:\/\/gitlab\.com\/[a-z0-9-_]+\/[a-z0-9-_]+\/-\/commits\/v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\s+)?/.test(readme)) {
    readme = readme.replace(/badges\/(\d+\.\d+\.\d+)(\.?[a-z]\.?\d+)?/, `badges/${pkg.version}`)
      .replace(/\/-\/commits\/v?(\d+\.\d+\.\d+)(\.?[a-z]\.?\s+)?/, `/-/commits/${pkg.version}`);
    console.log('Pipeline status badge updated');
  }

  // ![Maintenance status](https://img.shields.io/maintenance/<status>/<year>)
  if (/https:\/\/img\.shields\.io\/maintenance/.test(readme)) {
    readme = readme.replace(/https:\/\/img\.shields\.io\/maintenance\/(yes|no)\/\d{4}/, `https://img.shields.io/maintenance/yes/${new Date().getFullYear()}`);
    console.log('Maintenance status badge updated');
  }

  fs.writeFileSync('README.md', readme, {
    encoding: 'utf-8'
  });
}
