import { createMemoryHistory, createRouter } from 'vue-router';
import AuthView from '../views/AuthView.vue';
import WalletView from '../views/WalletView.vue';
import RoomView from '../views/RoomView.vue';
import TransferView from '../views/TransferView.vue';
import TransactionsView from '../views/TransactionsView.vue';
import AppointNewBankerView from '../views/AppointNewBankerView.vue';

const routes = [
  {
    path: '/',
    name: 'auth',
    component: AuthView
  },
  {
    path: '/wallet',
    name: 'wallet',
    component: WalletView
  },
  {
    path: '/room',
    name: 'room',
    component: RoomView
  },
  {
    path: '/transfer',
    name: 'transfer',
    component: TransferView
  },
  {
    path: '/transactions',
    name: 'transactions',
    component: TransactionsView
  },
  {
    path: '/appoint-new-banker',
    name: 'appoint-new-banker',
    component: AppointNewBankerView
  }
];

const router = createRouter({
  history: createMemoryHistory(),
  routes
});

export default router;
