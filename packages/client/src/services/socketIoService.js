import { io, Socket } from 'socket.io-client';
import { DateTime } from 'luxon';
import router from '@/router';
import { useGameStore } from '@/stores/game';
import { useNotificationStore } from '@/stores/notification';
import { useUserStore } from '@/stores/user';

let instance = null;

export default class SocketIoService {
  #socket;
  #playerId;
  #userStore;
  #gameStore;
  #notificationStore;
  #reconnectionAttempts = 0;
  #reconnectionInterval = null;

  constructor() {
    if (instance) return instance;
    instance = this;

    this.#socket = io({
      autoConnect: false,
      auth: {
        token: null
      },
      transports: ['websocket', 'polling'],
      path: '/game'
    });
    this.#userStore = useUserStore();
    this.#gameStore = useGameStore();
    this.#notificationStore = useNotificationStore();
    this.#init();
  }

  /**
   *
   * @returns {Socket}
   */
  get socket() {
    return this.#socket;
  }

  set playerId(playerId) {
    this.#playerId = playerId;
    this.#socket.auth.token = playerId;
  }

  #init() {
    this.#socket.on('connect', () => {
      console.log('Connected to server');
      if (this.#gameStore.inGame) {
        this.#reconnectionAttempts = 0;
        clearInterval(this.#reconnectionInterval);
        this.#reconnectionInterval = null;

        this.#socket.emit('login', {
          name: this.#userStore.name,
          room: this.#gameStore.room.code
        }, async error => {
          if (error) {
            console.error(error);
            this.#notificationStore.putNotification({
              content: 't:' + error.error,
              displayStyle: 'error'
            });
            await this.#logout();
          } else {
            this.#notificationStore.putNotification({
              content: 't:auth.reconnection.connected'
            });
          }
        });
      }
    });

    this.#socket.on('disconnect', () => {
      if (this.#gameStore.inGame) {
        console.log('in game, trying to reconnect');
        this.#reconnectionInterval = setInterval(() => this.#tryReconnect(), 5000);
      }
    });

    this.#socket.on('player:logged_in', (data) => {
      this.#gameStore.$patch({
        player: {
          id: data.player.id,
          funds: data.player.balance,
          isBanker: data.player.isBanker,
          isBankrupt: data.player.isBankrupt
        },
        room: {
          funds: data.player.isBanker ? data.room.funds : 0,
          transactions: data.room.transactions.map(tr => {
            tr.dateTime = DateTime.fromISO(tr.dateTime);
            return tr;
          }),
          players: data.room.players
        }
      });
    });

    this.#socket.on('room:new_transaction', (transaction) => {
      if (!this.#gameStore.hasTransaction(transaction.id)) {
        const notificationContent = this.#gameStore.addTransaction(transaction);
        this.#notificationStore.putNotification({
          content: notificationContent,
          type: 'transaction',
          displayStyle: 'success'
        });
      }
    });

    this.#socket.on('room:player:joined', (data) => {
      if (!this.#gameStore.hasPlayer(data.id)) {
        this.#gameStore.addPlayer(data);
      } else {
        this.#gameStore.updatePlayerName(data.id, data.name);
      }
    });

    this.#socket.on('room:player:bankrupt', async (player) => {
      if (this.#gameStore.hasPlayer(player.id)) {
        const user = this.#gameStore.bankruptPlayer(player.id);
        if (user.thisPlayer) {
          if (user.isBanker) {
            this.#notificationStore.putNotification({
              content: 't:notifications.room.bankrupt.this_banker'
            });
          } else {
            this.#notificationStore.putNotification({
              content: 't:notifications.room.bankrupt.this_player'
            });
            await this.#logout();
          }
        } else {
          this.#notificationStore.putNotification({
            content: 't:notifications.room.bankrupt.other_player',
            params: { username: user.name }
          });
        }
      }
    });

    this.#socket.on('room:destroyed', async () => {
      this.#notificationStore.putNotification({
        content: 't:notifications.room.destroyed'
      });
      await this.#logout();
    });

    this.#socket.on('room:new_banker_appointed', async (player) => {
      if (this.#gameStore.hasPlayer(player.id)) {
        if (this.#gameStore.player.id === player.id) {
          this.#gameStore.player.isBanker = true;
          this.#notificationStore.putNotification({
            content: 't:notifications.room.new_banker.you'
          });
        } else {
          const newBanker = this.#gameStore.room.players.find(plr => plr.id === player.id);
          this.#notificationStore.putNotification({
            content: 't:notifications.room.new_banker.other',
            params: {
              username: newBanker.name
            }
          });
          if (this.#gameStore.player.isBanker) {
            this.#gameStore.player.isBanker = false;
            if (this.#gameStore.player.isBankrupt) {
              this.#notificationStore.putNotification({
                content: 't:notifications.room.former_banker_bankrupt'
              });
              await this.#logout();
            } else {
              this.#gameStore.$patch({
                room: {
                  funds: 0,
                  transactions: this.#gameStore.playerTransactions
                }
              });
            }
          }
        }
      }
    });

    this.#socket.on('room:banker_data', async data => {
      this.#gameStore.$patch({
        room: {
          funds: data.room.funds,
          transactions: data.room.transactions.map(tr => {
            tr.dateTime = DateTime.fromISO(tr.dateTime);
            return tr;
          }),
          players: data.room.players
        }
      });
    });
  }

  async #tryReconnect() {
    if (!this.#gameStore.inGame) {
      clearInterval(this.#reconnectionInterval);
      this.#reconnectionInterval = null;
    }

    if (this.#reconnectionAttempts === 5) {
      clearInterval(this.#reconnectionInterval);
      this.#reconnectionInterval = null;
      this.#notificationStore.putNotification({
        content: 't:auth.reconnection.failed',
        displayStyle: 'error'
      });
      await this.#logout();
    } else if (!this.#socket.connected && !this.#socket.connecting) {
      this.#notificationStore.putNotification({
        content: 't:auth.reconnection.attempt',
        params: {
          attempt: this.#reconnectionAttempts + 1
        }
      });
      this.#socket.connect();
      this.#reconnectionAttempts++;
    }
  }

  async #logout() {
    this.#gameStore.$patch({
      inGame: false,
      player: {
        id: '',
        funds: 0,
        isBanker: false
      },
      room: {
        code: '',
        funds: 0,
        transactions: [],
        players: []
      }
    });
    await router.push('/');
    this.#socket.disconnect();
  }
}
