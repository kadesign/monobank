import { createApp } from 'vue';
import { createPinia } from 'pinia';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import {
  faArrowRightFromBracket,
  faArrowRightToBracket,
  faBuildingColumns,
  faCircleExclamation,
  faDoorOpen,
  faKey,
  faLink,
  faMoneyBillTransfer,
  faPersonWalkingArrowRight,
  faUser,
  faUserLock,
  faUserNinja,
  faUserPen,
  faWallet
} from '@fortawesome/free-solid-svg-icons';
import router from '@/router';
import App from './App.vue';
import i18n from '@/plugins/i18n';

const pinia = createPinia();
library.add(
  faArrowRightFromBracket,
  faArrowRightToBracket,
  faBuildingColumns,
  faCircleExclamation,
  faDoorOpen,
  faKey,
  faLink,
  faMoneyBillTransfer,
  faPersonWalkingArrowRight,
  faUser,
  faUserLock,
  faUserNinja,
  faUserPen,
  faWallet
);

createApp(App)
  .use(i18n)
  .use(pinia)
  .use(router)
  .component('font-awesome-icon', FontAwesomeIcon)
  .mount('#app');

router.replace('/');
