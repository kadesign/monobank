import { defineStore } from 'pinia';
import { DateTime } from 'luxon';

export const useGameStore = defineStore('gameState', {
  state: () => {
    return {
      inGame: false,
      player: {
        id: '',
        funds: 0,
        isBanker: false,
        isBankrupt: false
      },
      room: {
        code: '',
        funds: 0,
        transactions: [],
        players: []
      }
    }
  },
  getters: {
    playerTransactions(state) {
      return state.room.transactions
        .sort((a, b) => b.dateTime - a.dateTime)
        .filter(tr => (tr.from === this.player.id || tr.to === this.player.id));
    },
    lastPlayerTransactions(state) {
      return state.room.transactions
        .sort((a, b) => b.dateTime - a.dateTime)
        .filter(tr => (tr.from === this.player.id || tr.to === this.player.id))
        .slice(0, 5);
    },
    bankTransactions(state) {
      return state.room.transactions
        .sort((a, b) => b.dateTime - a.dateTime)
        .filter(tr => (tr.from === 'bank' || tr.to === 'bank'));
    }
  },
  actions: {
    hasTransaction(id) {
      return !!this.room.transactions.find(tr => tr.id === id);
    },
    addTransaction(transaction) {
      transaction.dateTime = DateTime.fromISO(transaction.dateTime);
      this.room.transactions.push(transaction);
      if (this.player.isBanker) {
        if (transaction.from === 'bank') {
          this.room.funds -= transaction.amount;
        }
        if (transaction.to === 'bank') {
          this.room.funds += transaction.amount;
        }
      }
      if (transaction.from === this.player.id) {
        this.player.funds -= transaction.amount;
      }
      if (transaction.to === this.player.id) {
        this.player.funds += transaction.amount;
      }
      return this.generateTransactionNotificationContent(transaction);
    },
    hasPlayer(id) {
      return !!this.room.players.find(plr => plr.id === id);
    },
    addPlayer(player) {
      this.room.players.push(player);
    },
    updatePlayerName(id, name) {
      const player = this.room.players.find(plr => plr.id === id);
      player.name = name;
    },
    bankruptPlayer(id) {
      const player = this.room.players.find(plr => plr.id === id);
      player.isBankrupt = true;
      if (this.player.id === id) {
        this.player.isBankrupt = true;
        return {
          thisPlayer: true,
          isBanker: this.player.isBanker
        };
      }
      return { name: player.name };
    },
    generateTransactionNotificationContent(transaction) {
      const senderName = transaction.from === 'bank'
        ? 'party.bank'
        : this.room.players.find(plr => plr.id === transaction.from).name;
      const recipientName = transaction.to === 'bank'
        ? 'party.bank'
        : this.room.players.find(plr => plr.id === transaction.to).name;
      const notificationContent = {
        amount: transaction.amount,
        direction: '',
        party: '',
        type: '',
        message: ''
      };

      if (transaction.description?.startsWith('t:') && this.player.isBanker) {
        notificationContent.type = transaction.description;
        if (transaction.description === 't:initial_funds') {
          notificationContent.direction = 'to';
          notificationContent.party = recipientName;
        } else if (transaction.description === 't:remaining_funds') {
          notificationContent.direction = 'from';
          notificationContent.party = senderName;
        }
      } else {
        notificationContent.message = transaction.description;
        if ([ senderName, recipientName ].includes('party.bank') && this.player.isBanker) {
          notificationContent.type = 'bank_transfer';
          if (senderName === 'party.bank') {
            notificationContent.direction = 'to';
            notificationContent.party = transaction.to === this.player.id ? 'party.you' : recipientName;
          } else if (recipientName === 'party.bank') {
            notificationContent.direction = 'from';
            notificationContent.party = transaction.from === this.player.id ? 'party.you' : senderName;
          }
        } else {
          if (transaction.from === this.player.id) {
            notificationContent.type = 'outgoing_transfer';
            notificationContent.direction = 'to';
            notificationContent.party = recipientName;
          } else if (transaction.to === this.player.id) {
            notificationContent.type = 'incoming_transfer';
            notificationContent.direction = 'from';
            notificationContent.party = senderName;
          }
        }
      }
      return notificationContent;
    }
  }
});
