import { defineStore } from 'pinia';
import { v4 as uuidV4 } from 'uuid';

export const useNotificationStore = defineStore('notificationsState', {
  state: () => {
    return {
      notifications: [],
      confirmation: {
        header: '',
        headerParams: {},
        text: '',
        textParams: {},
        acceptButtonText: '',
        acceptButtonStyle: '',
        declineButtonText: '',
        show: false,
        acceptAction: null,
        declineAction: null
      }
    };
  },
  actions: {
    putNotification({content, params, type = 'text', displayStyle = 'info'}) {
      const notification = { id: uuidV4(), type, content, params, displayStyle };
      this.notifications.unshift(notification);
      setTimeout(() => {
        const index = this.notifications.indexOf(notification);
        if (index >= 0) {
          this.notifications.splice(index, 1);
        }
      }, 5000);
    },
    dismissNotification(id) {
      const notification = this.notifications.find(ntf => ntf.id === id);
      if (notification) {
        this.notifications.splice(this.notifications.indexOf(notification), 1);
      }
    },
    showConfirmationDialog({ header, headerParams, text, textParams, acceptButtonText, acceptButtonStyle,
                             declineButtonText, acceptAction, declineAction }) {
      this.confirmation.header = header;
      this.confirmation.headerParams = headerParams;
      this.confirmation.text = text;
      this.confirmation.textParams = textParams;
      this.confirmation.acceptButtonText = acceptButtonText;
      this.confirmation.acceptButtonStyle = acceptButtonStyle;
      this.confirmation.declineButtonText = declineButtonText;
      this.confirmation.acceptAction = acceptAction || (() => {});
      this.confirmation.declineAction = declineAction || (() => {});
      this.confirmation.show = true;
    },
    hideConfirmationDialog() {
      this.confirmation.show = false;
    }
  }
});
