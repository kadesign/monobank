import { defineStore } from 'pinia';
import { v4 as uuidV4 } from 'uuid';

export const useUserStore = defineStore('currentUser', {
  state: () => {
    return {
      name: '',
      id: ''
    }
  },
  getters: {
    user(state) {
      return {
        name: state.name,
        id: state.id
      };
    }
  },
  actions: {
    load() {
      this.name = localStorage.getItem('name') || '';
      this.id = localStorage.getItem('id') || '';
      if (!this.id) {
        this.id = this.generateId();
        localStorage.setItem('id', this.id);
      }
    },
    save() {
      localStorage.setItem('name', this.name);
      localStorage.setItem('id', this.id);
    },
    generateId() {
      return uuidV4();
    }
  }
});
