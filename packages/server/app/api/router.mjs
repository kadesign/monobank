import express from 'express';

import RoomsApiRouter from './rooms/router.mjs';

export default class ApiRouter {
  #router;

  constructor() {
    this.#router = express.Router();
    this.#registerRoutes();
  }

  get router() {
    return this.#router;
  }

  #registerRoutes() {
    this.#router.use('/rooms', new RoomsApiRouter().router);
  }
}
