import EventEmitter from 'events';
import MicroLog from '@kadesign/microlog';

import Room from '../../models/room.mjs';

let instance = null;

export default class RoomManager extends EventEmitter {
  #rooms = [];
  #logger;

  constructor() {
    if (instance) return instance;

    super();
    instance = this;
    this.#logger = new MicroLog(this);
  }

  createRoom() {
    const room = new Room(this.#generateRoomCode());

    this.#rooms.push(room);
    this.#logger.log(`Room ${room.code} created`);
    return room.code;
  }

  findRoom(code) {
    return this.#rooms.find(room => room.code === code.toUpperCase());
  }

  findRoomOrThrow(code) {
    const room = this.findRoom(code);
    if (!room) throw new Error('errors.room.not_exists');

    return room;
  }

  joinRoom(code, name, token, socketId) {
    if (!name) throw new Error('errors.player.name_empty');

    const room = this.findRoomOrThrow(code);
    let player = room.findPlayerByToken(token);
    let initTransaction;
    if (!player) {
      if (room.hasPlayerByName(name)) throw new Error('errors.player.name_exists');

      player = room.addPlayer(name, token, socketId);
      initTransaction = this.#addTransaction(room, 'bank', player.id, player.balance, 't:initial_funds', true);
      this.#logger.log(`Player ${player.name} joined room ${room.code}`);
    } else {
      if (player.isBankrupt && !player.isBanker) throw new Error('errors.player.is_bankrupt');
      if (player.name !== name) {
        if (room.hasPlayerByName(name)) throw new Error('errors.player.name_exists');
        player.name = name;
      }
      player.socketId = socketId;
      this.#logger.log(`Player ${player.name} re-joined room ${room.code}`);
    }
    return {
      initTransaction,
      initState: {
        player: player.data,
        room: player.isBanker ? room.bankerData : room.getPlayerData(player.id)
      }
    }
  }

  destroyRoom(room) {
    this.#rooms.splice(this.#rooms.indexOf(room), 1);
    this.emit('room:destroyed', room);
    this.#logger.log(`Room ${room.code} destroyed`);
  }

  destroyRoomByCode(code, token) {
    const room = this.findRoomOrThrow(code);
    if (token === room.getBanker().token) {
      this.destroyRoom(room);
    } else {
      throw new Error('errors.room.destroy.not_banker');
    }
  }

  checkAvailability(code) {
    return !!this.findRoom(code.toUpperCase());
  }

  makeTransfer(roomCode, senderId, recipientId, amount, description) {
    const room = this.findRoomOrThrow(roomCode);
    const sender = senderId === 'bank' ? 'bank' : room.findPlayerById(senderId);
    const recipient = recipientId === 'bank' ? 'bank' : room.findPlayerById(recipientId);

    if (sender === 'bank') {
      room.subtractFunds(amount);
    } else {
      sender.subtractBalance(amount);
    }
    if (recipient === 'bank') {
      room.addFunds(amount);
    } else {
      recipient.addBalance(amount);
    }

    this.#addTransaction(room, senderId, recipientId, amount, description);
  }

  bankruptPlayer(roomCode, playerId, token) {
    const room = this.findRoomOrThrow(roomCode);
    const player = room.findPlayerById(playerId);
    if (player) {
      if (player.token === token) {
        const playerBalance = room.bankruptPlayer(player);
        this.emit('room:player_bankrupted', { room, playerId });
        this.#addTransaction(room, player.id, 'bank', playerBalance, 't:remaining_funds');
      } else {
        throw new Error('errors.player.invalid_token');
      }
    } else {
      throw new Error('errors.player.not_exists');
    }
  }

  appointNewBanker(roomCode, formerBankerId, newBankerId, formerBankerToken) {
    const room = this.findRoomOrThrow(roomCode);
    const formerBanker = room.findPlayerById(formerBankerId);
    if (formerBanker) {
      if (formerBanker.token === formerBankerToken) {
        const newBanker = room.findPlayerById(newBankerId);
        if (newBanker) {
          formerBanker.isBanker = false;
          newBanker.isBanker = true;
          this.emit('room:new_banker_appointed', { room, newBanker });
        } else {
          throw new Error('errors.player.new_banker_not_exists');
        }
      }
    } else {
      throw new Error('errors.player.former_banker_not_exists');
    }
  }

  #generateRoomCode() {
    const chars = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    let code = '';

    for (let i = 0; i < 4; i++) {
      code += chars[Math.floor(Math.random() * chars.length)];
    }

    return !this.findRoom(code) ? code : this.#generateRoomCode();
  }

  #addTransaction(room, from, to, amount, description, skipEmit = false) {
    const result = {
      room,
      transaction: room.addTransaction(from, to, amount, description)
    }
    if (skipEmit) {
      return result;
    } else {
      this.emit('room:new_transaction', result);
    }
  }
}
