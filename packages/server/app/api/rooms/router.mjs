import express from 'express';

import RoomManager from './roomManager.mjs';
import ErrorResponse from '../../models/response/errorResponse.mjs';
import PositiveResponse from '../../models/response/positiveResponse.mjs';

export default class RoomsApiRouter {
  #router;
  #roomMgr;

  constructor() {
    this.#router = express.Router();
    this.#roomMgr = new RoomManager();
    this.#registerRoutes();
  }

  get router() {
    return this.#router;
  }

  #registerRoutes() {
    this.#router.get('/create', (req, res) => {
      const roomCode = this.#roomMgr.createRoom();

      const response = new PositiveResponse({ code: roomCode });
      res.status(response.statusCode).json(response);
    });

    this.#router.get('/check-availability/:code', (req, res) => {
      let response;

      try {
        const availability = this.#roomMgr.checkAvailability(req.params.code);
        response = new PositiveResponse(availability);
      } catch (e) {
        console.error(`Unable to check room availability:`, e);
        response = new ErrorResponse(e.message);
      } finally {
        res.status(response.statusCode).json(response);
      }
    });
  }
}
