import { Server } from 'socket.io';

import RoomManager from '../api/rooms/roomManager.mjs';

let instance = null;

export default class WsServer {
  #socketIo;
  #roomManager;

  constructor(server) {
    if (instance) return instance;

    instance = this;

    this.#socketIo = new Server(server, { path: '/game' });
    this.#roomManager = new RoomManager();
    this.#init();
  }

  #init() {
    this.#socketIo.on('connection', socket => {
      console.log('Client connected');
      socket.on('disconnect', () => console.log('Client disconnected'));

      socket.on('login', async ({ room, name }, callback) => {
        try {
          const roomId = `room:${room}`;
          const gameData = this.#roomManager.joinRoom(room, name, socket.handshake.auth.token, socket.id);
          socket.join(roomId);
          await socket.emit('player:logged_in', gameData.initState);
          console.log('logged in');
          if (gameData.initTransaction) {
            this.#sendTransaction(gameData.initTransaction.room, gameData.initTransaction.transaction);
          }
          callback();
        } catch (e) {
          console.error(e);
          callback({ error: e.message });
          socket.disconnect();
        }
      });

      socket.on('transfer', async ({ room, sender, recipient, amount, description }, callback) => {
        try {
          this.#roomManager.makeTransfer(room, sender, recipient, amount, description);
          callback();
        } catch (e) {
          console.error(e);
          callback({ error: e.message });
        }
      });

      socket.on('appoint_banker', async ({ room, formerBankerId, newBankerId }, callback) => {
        try {
          this.#roomManager.appointNewBanker(room, formerBankerId, newBankerId, socket.handshake.auth.token);
          callback();
        } catch (e) {
          console.error(e);
          callback({ error: e.message });
        }
      });

      socket.on('bankrupt', async ({ room, playerId }, callback) => {
        try {
          this.#roomManager.bankruptPlayer(room, playerId, socket.handshake.auth.token);
          callback();
        } catch (e) {
          console.error(e);
          callback({ error: e.message });
        }
      });

      socket.on('delete_room', async ({ room }, callback) => {
        try {
          this.#roomManager.destroyRoomByCode(room, socket.handshake.auth.token);
          callback();
        } catch (e) {
          console.error(e);
          callback({ error: e.message });
        }
      });
    });

    this.#socketIo.of('/').adapter.on('create-room', roomId => {
      if (roomId.startsWith('room:')) {
        console.log(`Room socket ${roomId} created`);

        const roomCode = roomId.split(':')[1];
        const room = this.#roomManager.findRoom(roomCode);
        if (room) {
          const timerCleared = room.clearDestroyTimer();
          if (timerCleared) {
            console.log(`Room ${roomCode} will not be destroyed until there are no players online`);
          }
        }
      }
    });

    this.#socketIo.of('/').adapter.on('join-room', (roomId, playerSocketId) => {
      if (roomId.startsWith('room:')) {
        const roomCode = roomId.split(':')[1];
        const room = this.#roomManager.findRoom(roomCode);
        if (room) {
          const player = room.findPlayerBySocketId(playerSocketId);
          this.#socketIo.in(roomId).emit('room:player:joined', {
            id: player.id,
            name: player.name
          });
        }
      }
    });

    this.#socketIo.of('/').adapter.on('leave-room', (roomId, playerSocketId) => {
      if (roomId.startsWith('room:')) {
        const roomCode = roomId.split(':')[1];
        const room = this.#roomManager.findRoom(roomCode);
        if (room) {
          const player = room.findPlayerBySocketId(playerSocketId);
          this.#socketIo.in(roomId).emit('room:player:left', {
            id: player.id
          });
        }
      }
    });

    this.#socketIo.of('/').adapter.on('delete-room', roomId => {
      if (roomId.startsWith('room:')) {
        const roomCode = roomId.split(':')[1];
        const room = this.#roomManager.findRoom(roomCode);
        if (room) {
          room.setDestroyTimer(() => {
            this.#roomManager.destroyRoom(room);
          });
          console.log(`Room ${roomCode} will be automatically destroyed after 12 hours of inactivity`);
        }
        console.log(`Room socket ${roomId} deleted`);
      }
    });

    this.#roomManager.on('room:new_transaction', ({ room, transaction }) => {
      this.#sendTransaction(room, transaction);
    });

    this.#roomManager.on('room:destroyed', room => {
      this.#socketIo.to('room:' + room.code).emit('room:destroyed');
    });

    this.#roomManager.on('room:player_bankrupted', ({ room, playerId }) => {
      this.#socketIo.to('room:' + room.code).emit('room:player:bankrupt', { id: playerId });
    });

    this.#roomManager.on('room:new_banker_appointed', ({ room, newBanker }) => {
      this.#socketIo.to('room:' + room.code).emit('room:new_banker_appointed', { id: newBanker.id });
      this.#socketIo.to(newBanker.socketId).emit('room:banker_data', { room: room.bankerData });
    });
  }

  #sendTransaction(room, transaction) {
    const parties = new Set();
    const addParties = (...ids) => {
      for (let id of ids) {
        if (id === 'bank') {
          const banker = room.getBanker();
          parties.add(banker.socketId);
        } else {
          const player = room.findPlayerById(id);
          parties.add(player.socketId);
        }
      }
    };

    addParties(transaction.from, transaction.to);
    Array.from(parties.values()).forEach(party => {
      this.#socketIo.in(party).emit('room:new_transaction', transaction);
    });
  }
}
