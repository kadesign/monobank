import * as http from 'http';
import * as path from 'path';
import { URL, fileURLToPath } from 'url';
import express from 'express';

import ApiRouter from './api/router.mjs';
import WsServer from './ws/wsServer.mjs';

const PORT = process.env.PORT || 3000;
const app = express();

const server = http.createServer(app);
const apiRouter = new ApiRouter();

const staticPath = process.env.NODE_ENV === 'development' ? '../../client/dist' : './static';
const __dirname = fileURLToPath(new URL('.', import.meta.url));

app.use(express.json());
// Cache fonts for longer
app.use((req, res, next) => {
  if (req.method === 'GET' && /.*\/fonts\/.*/.test(req.url)) {
    const period = 60 * 60 * 24;
    res.set('Cache-control', `public, max-age=${period}`);
  }
  next();
});
app.use(express.static(path.join(__dirname, staticPath)));
app.use('/api', apiRouter.router);

new WsServer(server);

server.listen(PORT, () => console.log(`Listening on ${PORT}`));
