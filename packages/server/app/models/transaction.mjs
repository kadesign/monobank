import { v4 } from 'uuid';

export default class Transaction {
  #id;
  #from;
  #to;
  #amount;
  #description;
  #dateTime;

  constructor(from, to, amount, description = null) {
    this.#id = v4();
    this.#from = from;
    this.#to = to;
    this.#amount = amount;
    this.#description = description;
    this.#dateTime = new Date();
  }


  get from() {
    return this.#from;
  }

  get to() {
    return this.#to;
  }

  get amount() {
    return this.#amount;
  }

  get description() {
    return this.#description;
  }

  get dateTime() {
    return this.#dateTime;
  }

  toJSON() {
    return {
      id: this.#id,
      from: this.#from,
      to: this.#to,
      amount: this.#amount,
      description: this.#description,
      dateTime: this.#dateTime
    }
  }
}
