export default class Response {
  #ok;
  #data;
  #error;
  #statusCode;

  constructor(statusCode, ok, data = null, error = null) {
    this.#statusCode = statusCode;
    this.#ok = ok;
    this.#data = data;
    this.#error = error;
  }

  get statusCode() {
    return this.#statusCode;
  }

  toJSON() {
    const response = {
      ok: this.#ok
    };

    if (this.#ok) {
      response.data = this.#data;
    } else {
      response.error = this.#error;
    }

    return response;
  }
}
