import Response from './response.mjs';

export default class ErrorResponse extends Response {
  constructor(error) {
    super(500, false, null, error);
  }
}
