import Response from './response.mjs';

export default class PositiveResponse extends Response {
  constructor(data) {
    super(200, true, data);
  }
}
