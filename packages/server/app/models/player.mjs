import { v4 } from 'uuid';

export default class Player {
  name;
  socketId;
  isBanker;
  #isBankrupt;
  #id;
  #token;
  #balance;

  constructor(name, token, socketId, isBanker = false) {
    this.name = name;
    this.socketId = socketId;
    this.isBanker = isBanker;
    this.#id = v4();
    this.#token = token;
    this.#balance = 1500;
  }

  get id() {
    return this.#id;
  }

  get token() {
    return this.#token;
  }

  get balance() {
    return this.#balance;
  }

  get isBankrupt() {
    return this.#isBankrupt;
  }

  declareBankruptcy() {
    const oldBalance = this.#balance;
    this.#isBankrupt = true;
    this.#balance = 0;
    return oldBalance;
  }

  get data() {
    return {
      name: this.name,
      id: this.#id,
      balance: this.#balance,
      isBanker: this.isBanker,
      isBankrupt: this.isBankrupt
    }
  }

  addBalance(amount) {
    this.#balance += amount;
    return this.#balance;
  }

  subtractBalance(amount) {
    if (this.#balance < amount) {
      throw new Error('errors.transfer.insufficient_player_funds');
    }

    this.#balance -= amount;
    return this.#balance;
  }
}
