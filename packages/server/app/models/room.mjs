import Player from './player.mjs';
import Transaction from './transaction.mjs';

export default class Room {
  /**
   * @type {string}
   */
  #code;
  /**
   * @type {Player[]}
   */
  #players = [];
  /**
   * @type {number}
   */
  #funds;
  /**
   * @type {Transaction[]}
   */
  #transactions = [];
  /**
   * @type {Timeout|null}
   */
  #destroyTimer = null;

  /**
   * @param {string} code
   */
  constructor(code) {
    this.#code = code;
    this.#funds = 20580;
  }

  /**
   * @returns {string}
   */
  get code() {
    return this.#code;
  }

  /**
   * @returns {number}
   */
  get funds() {
    return this.#funds;
  }

  /**
   * @returns {{players: {name: string, id: string}[], funds, transactions: Transaction[]}}
   */
  get bankerData() {
    return {
      funds: this.#funds,
      transactions: this.#transactions,
      players: this.#players.map(player => ({
        id: player.id,
        name: player.name,
        isBankrupt: player.isBankrupt
      }))
    }
  }

  /**
   * @param {function} callback
   */
  setDestroyTimer(callback) {
    if (this.#destroyTimer) clearTimeout(this.#destroyTimer);
    this.#destroyTimer = setTimeout(callback, 12 * 60 * 60 * 1000);
  }

  /**
   * @returns {boolean}
   */
  clearDestroyTimer() {
    if (this.#destroyTimer) {
      clearTimeout(this.#destroyTimer);
      this.#destroyTimer = null;
      return true;
    } else {
      return false;
    }
  }

  /**
   * @param {string} id
   * @returns {{players: {name: string, id: string}[], transactions: Transaction[]}}
   */
  getPlayerData(id) {
    return {
      players: this.#players.map(player => ({
        id: player.id,
        name: player.name,
        isBankrupt: player.isBankrupt
      })),
      transactions: this.getTransactionsByPlayer(id)
    }
  }

  /**
   * @param {number} amount
   * @returns {number}
   */
  addFunds(amount) {
    this.#funds += amount;
    return this.#funds;
  }

  /**
   * @param {number} amount
   * @returns {number}
   * @throws {Error}
   */
  subtractFunds(amount) {
    if (this.#funds < amount) {
      throw new Error('errors.transfer.insufficient_room_funds');
    }

    this.#funds -= amount;
    return this.#funds;
  }

  /**
   * @param {string} name
   * @param {string} token
   * @param {string} socketId
   * @returns {Player}
   */
  addPlayer(name, token, socketId) {
    const player = new Player(name, token, socketId, this.#players.length === 0);
    this.subtractFunds(player.balance);
    this.#players.push(player);
    return player;
  }

  bankruptPlayer(player) {
    const playerBalance = player.declareBankruptcy();
    this.addFunds(playerBalance);
    return playerBalance;
  }

  /**
   * @param {string} id
   * @returns {Player|undefined}
   */
  findPlayerById(id) {
    return this.#players.find(plr => plr.id === id);
  }

  /**
   * @param {string} socketId
   * @returns {Player|undefined}
   */
  findPlayerBySocketId(socketId) {
    return this.#players.find(plr => plr.socketId === socketId);
  }

  /**
   * @param {string} token
   * @returns {Player|undefined}
   */
  findPlayerByToken(token) {
    return this.#players.find(plr => plr.token === token);
  }

  /**
   * @param {string} name
   * @returns {boolean}
   */
  hasPlayerByName(name) {
    return !!this.#players.find(plr => plr.name === name);
  }

  /**
   * @param {string} id
   * @returns {Transaction[]}
   */
  getTransactionsByPlayer(id) {
    return this.#transactions.filter(tr => tr.from === id || tr.to === id);
  }

  /**
   * @param {string} from
   * @param {string} to
   * @param {number} amount
   * @param {string?} description
   * @returns {Transaction}
   */
  addTransaction(from, to, amount, description) {
    const transaction = new Transaction(from, to, amount, description);
    this.#transactions.push(transaction);
    return transaction;
  }

  /**
   * @returns {Player|null}
   */
  getBanker() {
    const found = this.#players.filter(plr => plr.isBanker);
    if (found.length > 0) return found[0];
    return null;
  }
}
